## Git-Based Modpack Manager
---

Script to update modpacks automatically

### Installation

#### For *Windows* users:

1. Close *Minecraft* and *Minecraft Launcher*.

2. Download the latest installer from [the releases page](https://gitlab.com/1F335/modpackman/-/releases) and run it, completing the Forge prompt.

3. Start *Minecraft Launcher* and launch the newly installed modpack profile.


#### For other platforms:

1. Install Git, Python 3, Java, and *Minecraft*.

2. Close *Minecraft* and *Minecraft Launcher*.

3. Install the Python `requests` module with `pip install requests` (or `pip install --user requests` if the first one fails).

4. Clone or download this repository.

5. In a shell, navigate to the directory of your desired pack (e.g. `cd packs/jeffrey-3` for the J.E.F.F.R.E.Y. 3 modpack).

6. Run the installer with `python ../../installer.py`

7. Start *Minecraft Launcher* and launch the newly installed modpack profile.


### Maintenance:

To select a pack to work on, navigate to its directory (e.g. `cd packs/jeffrey-3` for the J.E.F.F.R.E.Y. 3 modpack).

Run `python ../../modpackman.py apply_updates` to update `pack-lock.ini` with the most recent compatible versions of every mod specified in this pack's `pack.ini`.  This will also update the list of bundled config files and increment the pack version.

Run `python ../../modpackman.py check_updates` to print out available updates for all the mods specified in this pack.

To bypass everything except the mod downloads, run `python ../../modpackman.py install`.  This will install all the mods specified in this pack's `pack-lock.ini`

***NOTE***: `check_updates` and `apply_updates` require you to have the `selenium` module installed


### Configuration:

All of the data related to specific pack is stored in its folder in `packs/`.  This includes:
 - The icon for the launcher profile (`icon.png`)
 - The icon for the executable installer (`icon.ico`)
 - The default mod config files (`config/`)
 - The modpackman pack configuration (`pack.ini`)
 - The current pack version information (`pack-lock.ini`)

Note: you can create a file `local-config.ini` in this folder on your local machine that will override any specified values in `pack.ini`

`pack.ini` has two sections:

 - `pack`, with these options:
   - `name`: the user-friendly name of the modpack
   - `pack_base_url`: the web url from whence the pack's data may be retrieved
   - `forge_url`: the download url for the forge installer for this pack. (note: this skips the Forge ads, consider supporting the project directly instead)
   - `game_version`: the maximum *Minecraft* version to update mods to
   - `java_args`: Java arguments to be added to the pack launcher profile when it is created.

 - `mods`, which is a collection of `mod_name = download_url` pairs.  For mods hosted on curseforge, the download url is the project's homepage url.

